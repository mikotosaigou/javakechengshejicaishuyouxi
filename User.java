/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author spring
 */
public class User {
	private String username;
	private int count;
	
	public User(String username, int l) {
		this.username = username;
		this.count = l;
	}
	
	

	public String getUsername() {
		return username;
	}



	public int getCount() {
		return count;
	}



	@Override
	public String toString() {
		return username + " " + count;
	}
}

    

