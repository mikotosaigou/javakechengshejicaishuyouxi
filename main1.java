import java.util.Scanner;

public class main1 {
    public static void main(String[] args) {
        
        int b = (int) (Math.random()*100 + 1);
        while (true) {
            try {
                Scanner in=new Scanner(System.in);
                int a=in.nextInt();
                if(a < 1 || a > 100) {
                    System.out.println("超出范围");
                } else if(a > b) {
                    System.out.println(a + "太大");
                } else if (a < b) {
                    System.out.println(a + "太小");
                } else {
                    System.out.println("猜对了");
                    break;
                }
            } catch(Exception e) {
                System.out.println("数字格式不合法");
                continue;
            }
        }
    }
}